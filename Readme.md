# Installation Guide

Run "yarn install" after cloning the project

# Required Tools

Node

Yarn

Nodemon

# Template Usage Guide

yarn start - Run Development Build

yarn production - Run Production Build

yarn build - Build Production Files

# Using Bot Framework Emulator

Connect to http://localhost:9000/api/messages

# Tool for testing the project

Bot Framework Emulator - https://github.com/Microsoft/BotFramework-Emulator