import restify from 'restify';
import * as builder from 'botbuilder';

const connector = new builder.ChatConnector({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
});

const bot = new builder.UniversalBot(connector);

bot.dialog('/', (session) => {
    session.send('Hello There!');
});

/**
 * Restify Config
 */
const server = restify.createServer();

server.listen(process.env.port || process.env.PORT || 9000, () => {
   console.log('%s listening to %s', server.name, server.url);
});

// Listen for messages from users
server.post('/api/messages', connector.listen());

